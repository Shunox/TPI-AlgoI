#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(grabacionValidaTEST, unaValida) {
    int freq = 4;
    int prof = 16;
    audio a = {1,2,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(true,result);
}

TEST(grabacionValidaTEST, otraValida) {
    int freq = 12;
    int prof = 32;
    audio a = {1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(true,result);
}

//creados

TEST(grabacionValidaTEST, frecuenciaInvalida) {
    int freq = 3;
    int prof = 32;
    audio a = {1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}


TEST(grabacionValidaTEST, profundidadInvalida) {
    int freq = 12;
    int prof = 20;
    audio a = {1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}


TEST(grabacionValidaTEST, audioConValoresNegativos) {
    int freq = 12;
    int prof = 32;
    audio a = {-1,2,-3,4,-5,6,-1,2,-3,4,-5,6,-1,2,-3,4,-5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(true,result);
}


TEST(grabacionValidaTEST, duraMenosDeUno) {
    int freq = 22;
    int prof = 32;
    audio a = {1,2,3,4,5,6,1,2,3,4,5,6,1,2,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}


TEST(grabacionValidaTEST, fueraDeRango) {
    int freq = 12;
    int prof = 16;
    audio a = {1,2,-3,4,5,6,-32769,2,3,-4,5,6,32768,2,3,4,5,-6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}


TEST(grabacionValidaTEST,subsecDeCerosMenorQueUnSegundo) {
    int freq = 4;
    int prof = 32;
    audio a = {1,2,3,4,5,0,0,2,3,4,5,0,0,0,3,4,5,6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(true,result);
}


TEST(grabacionValidaTEST,subsecDeCerosMayorQueUnSegundo) {
    int freq = 4;
    int prof = 32;
    audio a = {1,2,3,4,5,0,0,2,3,4,5,0,0,0,0,0,0,0};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}

TEST(grabacionValidaTEST, todosCeros) {
    int freq = 4;
    int prof = 32;
    audio a = {0,0,0,0};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}

TEST(grabacionValidaTEST, ) {
    int freq = 4;
    int prof = 16;
    audio a = {-10,20,800,96,-32769};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}

TEST(grabacionValidaTEST, todosProblemas) {
    int freq = 4;
    int prof = 16;
    audio a = {1,2,-3,4,5,6,-32769,2,3,-4,5,0,0,0,0,4,0,-6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false,result);
}
TEST(grabacionValidaTEST, variado) {
    int freq = 4;
    int prof = 16;
    audio a = {1,232,-3,4,552,6,-32,2,3,-4,5,6,0,-200,0,0,-323,-6};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(true,result);
}


TEST(grabacionValidaTEST,andaDespuesNO) {
    int freq = 4;
    int prof = 32;
    audio a = {1, 2, 3, 4, 5, 0, 0, 0, 0, 0, 5, 10, 0, -30, 2, 20, 0, 0};
    bool result = grabacionValida(a,prof,freq);
    EXPECT_EQ(false, result);

}
