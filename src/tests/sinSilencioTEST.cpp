#include "../Funciones_TPI.h"
#include "../ejercicios.h"

#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(sinSilenciosTEST,unAudio){
    int prof = 16;
    int freq = 35;
    int umbral = 1;
    audio a = {0,0,0,0,1,2,3,0,0,0,0,0,5,6,0,0,3};
    audio b = {1,2,3,5,6,0,0,3};

    audio b_out = sinSilencios(a,freq,prof,umbral);

    ASSERT_EQ(b.size(),b_out.size());
    for(int i=0;i<b.size();i++){

        EXPECT_EQ(b[i],b_out[i]);

    }


}

TEST(sinSilenciosTEST,conSilencioAlFinal){

    int prof = 32;
    int freq = 15;
    int umbral = 4;
    audio a = {0,0,0,0,9,25,30,300,4,9,10,0,2,3,0,0,0};
    audio b = {9,25,30,300,4,9,10};

    audio b_out = sinSilencios(a,freq,prof,umbral);

    ASSERT_EQ(b.size(),b_out.size());
    for(int i=0;i<b.size();i++){

        EXPECT_EQ(b[i],b_out[i]);

    }
}

TEST(sinSilenciosTEST,silencioAlPrincipio){
    int prof = 16;
    int freq = 16;
    int umbral = 2;
    audio a = {0,0,0,0,1,2,3,5,40,33,8,22,25,21,9,10,3,3,4,5,9,2,7,8,2,12,7,23,3,88};
    audio b = {2,3,5,40,33,8,22,25,21,9,10,3,3,4,5,9,2,7,8,2,12,7,23,3,88};

    audio b_out = sinSilencios(a,freq,prof,umbral);

    ASSERT_EQ(b.size(),b_out.size());
    for(int i=0;i<b.size();i++){

        EXPECT_EQ(b[i],b_out[i]);

    }
}