#include "../Funciones_TPI.h"
#include "../ejercicios.h"

#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(compararSilenciosTEST,archivoSpkr0Audio) {

    int prof;
    int freq;
    int duracion;
    int locutor;
    string archivo;
    audio audioDatos;


    locutor = 0;
    archivo = "datos/spkr" + to_string(locutor) + ".dat";
    audioDatos = leerVectorAudio(archivo,freq,prof,duracion);


    int umbral = 10000;

    float F1 = compararSilencios(audioDatos,freq,prof,locutor,umbral);

    EXPECT_NEAR(1.00, F1, 0.2);


}