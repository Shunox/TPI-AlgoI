#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(flashElPerezosoTEST, unCasoImpar) {
    int prof = 32;
    int freq = 4;
    sala m = {
            {-20, 30, 8, -1, 0, 10, 9},
            {11, 20, 31, -44, -10, -100, 40},
            { 2, 4, 3, 0, 1, 0, 0}
    };
    sala res = {
            {-20, 5, 30, 19, 8, 3, -1, 0, 0, 5, 10, 9, 9},
            {11, 15, 20, 25, 31, -6, -44, -27, -10, -55, -100, -30, 40},
            {2, 3, 4, 3, 3, 1, 0, 0, 1, 0, 0, 0, 0}
    };

    sala m_out = flashElPerezoso(m, prof, freq);

    for (int i = 0; i < m.size(); i++) {
        for (int j = 0; j < res[i].size(); j++) {
            EXPECT_EQ(res[i][j], m_out[i][j]);
        }
    }
}

TEST(flashElPerezosoTEST, audioCuadrado) {
    int prof = 16;
    int freq = 4;
    sala m = {
            {4,2,1},
            {6,9,4},
            {2,7,5}
    };
    sala res = {
            {4,3,2,1,1},
            {6,7,9,6,4},
            {2,4,7,6,5}
    };

    sala m_out = flashElPerezoso(m,prof,freq);
    for(int i=0;i<m.size();i++){
        for(int j=0;j<res[i].size();j++) {
            EXPECT_EQ(res[i][j],m_out[i][j]);
        }
    }
}

TEST(flashElPerezosoTEST, todoIgual) {
    int prof = 16;
    int freq = 10;
    sala m = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {3, 9, 0, 1, 0, 0, 0, 0, 0, 5, 4, 3, 1},
    };
    sala res = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {3, 6, 9, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 5, 4, 4, 3, 3, 2, 1},
    };

    sala m_out = flashElPerezoso(m, prof, freq);
    for (int i = 0; i < m.size(); i++) {
        for (int j = 0; j < res[i].size(); j++) {
            EXPECT_EQ(res[i][j], m_out[i][j]);
        }
    }
}