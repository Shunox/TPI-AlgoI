#include "../Funciones_TPI.h"
#include "../ejercicios.h"

#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(resultadoFinalTEST,todosLosAudios) {

    int prof;
    int freq;
    int duracion;
    int locutor;
    string archivo;
    audio audioDatos;

    sala audiosSalaDatos;

    for (int i=0;i<6;i++){
        locutor = i;
        archivo = "datos/spkr" + to_string(locutor) + ".dat";
        audioDatos = leerVectorAudio(archivo,freq,prof,duracion);
        audiosSalaDatos.push_back(audioDatos);
    }

    int umbral = 7500;

    float F1_Promedio = resultadoFinal(audiosSalaDatos,freq,prof,umbral);

    EXPECT_NEAR(1.00, F1_Promedio, 0.2);


}