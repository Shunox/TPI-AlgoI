#include "../Funciones_TPI.h"
#include "../ejercicios.h"

#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(silenciosTEST,unAudio){
    int prof = 16;
    int freq = 35;
    audio a = {0,0,0,0,1,2,3,0,0,0,0,0,5,6,0,0,3};
    lista_intervalos ts = {make_tuple(0,0.1143),make_tuple(0.2,0.3429)};

    lista_intervalos t_out = silencios(a,prof,freq,1);
    cout << "encontrados " << ts.size() << " intervalos" << endl;
    ASSERT_EQ(ts.size(),t_out.size());
    for(int i=0;i<ts.size();i++){
        intervalo t1 = ts[i];
        intervalo t2 = t_out[i];

        // test inicio del intervalo
        int m1 = redondear(get<0>(t1) * freq);
        int m2 = redondear(get<0>(t2) * freq);

        EXPECT_EQ(m1,m2);
        // test fin de intervalo
        m1 = redondear(get<1>(t1) * freq);
        m2 = redondear(get<1>(t2) * freq);

        EXPECT_EQ(m1,m2);

    }


}


TEST(silenciosTEST,conSilencioAlFinal){
    int prof = 32;
    int freq = 15;
    audio a = {0,0,0,0,9,25,30,300,4,9,10,0,2,3,0,0,0};
    lista_intervalos ts = {make_tuple(0,0.2667), make_tuple(0.7334,1.1334)} ;

    lista_intervalos t_out = silencios(a,prof,freq,4);
    cout << "encontrados " << ts.size() << " intervalos" << endl;
    ASSERT_EQ(ts.size(),t_out.size());
    for(int i=0;i<ts.size();i++){
        intervalo t1 = ts[i];
        intervalo t2 = t_out[i];

        // test inicio del intervalo
        int m1 = redondear(get<0>(t1) * freq);
        int m2 = redondear(get<0>(t2) * freq);

        EXPECT_EQ(m1,m2);
        // test fin de intervalo
        m1 = redondear(get<1>(t1) * freq);
        m2 = redondear(get<1>(t2) * freq);

        EXPECT_EQ(m1,m2);

    }
}

TEST(silenciosTEST,muchosSilenciosAcumulados){
    int prof = 16;
    int freq = 31;
    audio a = {0,0,0,0,1,2,3,7,-1,0,1,-1,5,6,0,0,1,0,7,6,0,-1,-1,-1,0,10,7,5,4,0,0,0,0};
    lista_intervalos ts = {make_tuple(0,0.1613), make_tuple(0.2581,0.3871), make_tuple(0.4517,0.5807), make_tuple(0.6452,0.8065), make_tuple(0.9355,1.0646)} ;

    lista_intervalos t_out = silencios(a,prof,freq,2);
    cout << "encontrados " << ts.size() << " intervalos" << endl;
    ASSERT_EQ(ts.size(),t_out.size());
    for(int i=0;i<ts.size();i++){
        intervalo t1 = ts[i];
        intervalo t2 = t_out[i];

        // test inicio del intervalo
        int m1 = redondear(get<0>(t1) * freq);
        int m2 = redondear(get<0>(t2) * freq);

        EXPECT_EQ(m1,m2);
        // test fin de intervalo
        m1 = redondear(get<1>(t1) * freq);
        m2 = redondear(get<1>(t2) * freq);

        EXPECT_EQ(m1,m2);

    }
}

TEST(silenciosTEST,silencioAlPrincipio){
    int prof = 16;
    int freq = 16;
    audio a = {0,0,0,0,1,2,3,5,40,33,8,22,25,21,9,10,3,3,4,5,9,2,7,8,2,12,7,23,3,88};
    lista_intervalos ts = {make_tuple(0,0.3125)} ;

    lista_intervalos t_out = silencios(a,prof,freq,2);
    cout << "encontrados " << ts.size() << " intervalos" << endl;
    ASSERT_EQ(ts.size(),t_out.size());
    for(int i=0;i<ts.size();i++){
        intervalo t1 = ts[i];
        intervalo t2 = t_out[i];

        // test inicio del intervalo
        int m1 = redondear(get<0>(t1) * freq);
        int m2 = redondear(get<0>(t2) * freq);

        EXPECT_EQ(m1,m2);
        // test fin de intervalo
        m1 = redondear(get<1>(t1) * freq);
        m2 = redondear(get<1>(t2) * freq);

        EXPECT_EQ(m1,m2);

    }
}

TEST(silenciosTEST,todoSilencio) {
    int prof = 16;
    int freq = 35;
    audio a = {0, 2, 0, -3, 1, 2, 3, 0, 0, -2, 0, 0, 5, 6, 0, 0, 3};
    lista_intervalos ts = {make_tuple(0, 0.4858)};

    lista_intervalos t_out = silencios(a, prof, freq, 9);
    cout << "encontrados " << ts.size() << " intervalos" << endl;
    ASSERT_EQ(ts.size(), t_out.size());

    for(int i=0;i<ts.size();i++){
        intervalo t1 = ts[i];
        intervalo t2 = t_out[i];

        // test inicio del intervalo
        int m1 = redondear(get<0>(t1) * freq);
        int m2 = redondear(get<0>(t2) * freq);

        EXPECT_EQ(m1,m2);
        // test fin de intervalo
        m1 = redondear(get<1>(t1) * freq);
        m2 = redondear(get<1>(t2) * freq);

        EXPECT_EQ(m1,m2);

    }
}

