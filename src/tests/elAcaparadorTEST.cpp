#include "../ejercicios.h"
#include "gtest/gtest.h"

TEST(elAcaparadorTEST, prueba){
    sala m = {
            {1,2,3,4,5,6},
            {0,0,8,6,7,8},
            {10,0,0,9,4,5},
            {5,7,8,9,2,1},
    };
    int freq = 3;
    int prof = 4;
    EXPECT_EQ(3, elAcaparador(m, freq, prof));
}

TEST(elAcaparadorTEST, prueba2){
    sala m = {
            {-1,2,-3,4,5,6,20},
            {0,0,-8,6,7,8,30},
            {10,0,0,9,4,5,-55},
            {5,7,8,9,2,1,-60},
    };
    int freq = 6;
    int prof = 16;
    EXPECT_EQ(3, elAcaparador(m, freq, prof));
}

TEST(elAcaparadorTEST, casoNegativo){
    sala m = {
            {-1,-2,-3,-4,-5,-6},
            {0,0,-8,-6,-7,-8},
            {-10,0,0,-9,-4,-5},
            {-5,-7,-8,-9,-2,-1},
    };
    int freq = 4;
    int prof = 32;
    EXPECT_EQ(3, elAcaparador(m, freq, prof));
}

TEST(elAcaparadorTEST, casoBordeDeCero){
    sala m = {
            {1,2,3,4,5,-1,-1,-1,2},
            {0,0,0,6,7,8,2,4,3,1},
            {0,0,0,2,100,5,80,56,7},
            {5,0,0,0,2,1,-8,-9-50},
    };
    int freq = 6;
    int prof = 16;
    EXPECT_EQ(2, elAcaparador(m, freq, prof));
}


TEST(elAcaparadorTEST, casoSInCeros){
    sala m = {
            {100,2,3,4,5,600},
            {2,1,1,1,2,3},
            {10,1,1,9,4,5},
            {9,56,6,44,2,11},
    };
    int freq = 5;
    int prof = 16;
    EXPECT_EQ(0, elAcaparador(m, freq, prof));
}

TEST(elAcaparadorTEST, CasoLimiteDeFrecuencia){
    sala m = {
            {1,2,3,4},
            {8,6,7,80},
            {0,0,4,5},
            {9,7,8,1},
    };
    int freq = 4;
    int prof = 16;
    EXPECT_EQ(1, elAcaparador(m, freq, prof));
}
