#ifndef SALADEREUNION_EJERCICIOS_H
#define SALADEREUNION_EJERCICIOS_H

#include <iostream>
#include <vector>
#include <math.h>
#include <tuple>
#include "definiciones.h"

using namespace std;

// Funciones Principales

bool grabacionValida(audio s, int prof, int freq);
int elAcaparador(sala m, int freq, int prof);
sala ardillizar(sala m, int prof, int freq);
sala flashElPerezoso(sala m, int prof, int freq);
lista_intervalos silencios(audio s, int prof, int freq, int umbral);
bool hayQuilombo(sala m, int prof, int freq, int umbral);


float compararSilencios(audio vec, int freq, int prof, int locutor, int umbralSilencio);
float resultadoFinal(sala m, int freq, int prof, int umbralSilencio);

audio sinSilencios(audio s, int freq, int prof, int umbral);

int encontrarAparicion(audio x, audio y);

locutor medirLaDistancia(sala m, audio frase, int freq, int prof);

//Funciones Auxiliares

int intensidadMedia(audio a);

bool freqValida(int freq);

bool profValida(int prof);

bool duraMasDe(audio s, int freq);

bool enRango(audio s, int prof);

bool micFunciona(audio a, int freq);

bool esParteSilencio(audio p,int k, int prof, int freq, int umbral);

vector<bool> enmascarar(int duracion, lista_intervalos tiempos);

void negacionLogica(vector<bool> &mascara);

float redondear(float d);

float enSegundos(int n, int freq);

float precision(float tiempo, int p);

float distanciasAP(sala m, int p1,int p2, int freq, audio frase);

#endif //SALADEREUNION_EJERCICIOS_H
