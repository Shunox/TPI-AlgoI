/*  TPI - Sala De Reuniones -- Algoritmos I
 *
 *  Grupo 2 ------------ 2017
 *
 */

#include <iomanip>
#include "ejercicios.h"
#include "Funciones_TPI.h"
#include "math.h"

/************************** EJERCICIO grabacionValida **************************/
bool grabacionValida(audio s, int prof, int freq) {
    bool verificacion = freqValida(freq) && profValida(prof) && duraMasDe(s,freq) && enRango(s, prof) && micFunciona(s,freq);

    return verificacion;
}


bool freqValida(int freq) {
    if (freq >= 4) {
        return true;
    } else {
        return false;
    }

}

bool profValida(int prof) {
    if (prof == 16 || prof == 32) {
        return true;
    } else {
        return false;
    }
}

bool duraMasDe(audio s, int freq) {
    float largo = s.size();
    float duracion = largo / freq;

    if (duracion > 1.0) {
        return true;
    } else {
        return false;
    }
}

bool enRango(audio s, int prof) {
    int inferior = -pow(2, prof - 1);
    int superior = pow(2, prof - 1) - 1;
    bool res = true;
    int i = 0;
    while (i < s.size()) {
        if (s[i] >= inferior && s[i] <= superior) {
            res = res && true;
        } else {
            res = false;
        }
        i++;
    }
    return res;
}

bool micFunciona(audio s, int freq) {
    int ultimoIndice = 0;
    bool res = true;

    for (int i = ultimoIndice; i<s.size()-1 ; ++i) {
        if (s[i] == 0) {

            for (int j = i + 1; j < s.size() && s[j]==0; j++) {
                if (j-i+1>=1.0*freq){
                    res = false;

                } else {
                    ultimoIndice = j;
                }
            }

        }

    }
    return res;

}

/************************** EJERCICIO elAcaparador **************************/
int elAcaparador(sala m, int freq, int prof) {
    int acaparador = 0;
    for (int i = 0; i < m.size(); i++) {
        if (intensidadMedia(m[acaparador]) < intensidadMedia(m[i])) {
            acaparador = i;
        }
    }

    return acaparador;
}

int intensidadMedia(audio a) {
    int suma = 0;
    int intensidadMedia=0;
    int largoAudio= a.size();
    for(int i=0;i<a.size();i++){
        suma += abs(a[i]);
    }
    intensidadMedia= suma /largoAudio;
    return intensidadMedia;
}

/************************** EJERCICIO ardillizar **************************/
sala ardillizar(sala m, int prof, int freq){
    sala res;
    for(int i=0; i<m.size();i++){
        res.push_back({});
        for(int j=0; j<m[i].size();j+=2){
            res[i].push_back(m[i][j]);
        }
    }
    return res;
}

/************************** EJERCICIO flashElPerezoso **************************/
sala flashElPerezoso(sala m, int prof, int freq){
    sala res;
    for(int i=0;i<m.size();i++){
        res.push_back({});
        for(int j=0;j<m[i].size();j++){
            res[i].push_back(m[i][j]);
            if(j!=m[i].size()){
            res[i].push_back(0);
            }
        }
    }
    for (int i = 0; i < res.size() ; ++i) {
        for (int j = 1; j < res[i].size()-1 ; j+=2) {
            res[i][j]=(res[i][j-1]+res[i][j+1])/2;
        }
    }
    return res;
}

/************************** EJERCICIO silencios **************************/
lista_intervalos silencios(audio s, int prof, int freq, int umbral){
    lista_intervalos res;
    bool esSilencio=true;
    double ti=0;
    double tf=0;


    for (int i=0;i<s.size();i++){
        if ( abs(s[i]) < umbral && (i==0 || abs(s[i - 1]) >= umbral) ) {
            for (int j = i + 0.1*freq; j < s.size(); j++) {
                esSilencio = true;

                while(j != (s.size() - 1) && abs(s[j + 1]) < umbral){
                        j++;
                }

                for (int k = i; k <= j && esSilencio; k++) {
                    esSilencio = esSilencio && abs(s[k]) < umbral;
                }

                if (esSilencio) {
                    ti = precision(enSegundos(i, freq), 4);
                    tf = precision(enSegundos(j + 1, freq), 4);
                    res.push_back(make_tuple(ti, tf));
                    //Buscar otros tiempos
                    i = j;
                    j = s.size();
                } else{
                    j=s.size();

                    }


            }
        }
    }
    return res;
}

float redondear(float d){
    d+=0.50;
    float r=0.00;
    for (r=0.00;r<=d;r++){

    }
    r-=1.00;
    int y = r;
    float res = y;
    return res;
}
float enSegundos(int n, int freq){

    float tiempo = (float) n / (float) freq;

    return tiempo;
}
float precision(float tiempo, int p){

    float tiempopreciso = redondear(tiempo * (pow(10,p))) / (pow(10,p));

    return tiempopreciso;
}

/************************** EJERCICIO hayQuilombo **************************/
bool hayQuilombo(sala m, int prof, int freq, int umbral){
    bool hayQuilombo=false;
    for(int i = 0;i < m.size()-1;i++){

        for (int j=i+1;j<m.size();j++){

            for(int k=0;k<m[i].size();k++){
               hayQuilombo = hayQuilombo || !(esParteSilencio(m[i],k,prof,freq,umbral) ||
                                              esParteSilencio(m[j],k,prof,freq,umbral));

            }
        }
    }

    return hayQuilombo;
}

bool esParteSilencio(audio p,int k, int prof, int freq, int umbral){
    lista_intervalos silenciosP1;
    int ti;
    int tf;
    bool perteneceSilencio=false;
    silenciosP1 = silencios(p,prof,freq,umbral);
    for(int t=0;t<silenciosP1.size();t++){
        ti = get<0>(silenciosP1[t])*freq;
        tf = get<1>(silenciosP1[t])*freq;
        perteneceSilencio = perteneceSilencio || (k>=ti && k<=tf);
    }
    return perteneceSilencio;
}

/************************** EJERCICIO compararSilencios **************************/

float compararSilencios(audio vec, int freq, int prof, int locutor, int umbralSilencio){

    string path= "datos/habla_spkr" + to_string(locutor) + ".txt";

    ifstream fl(path);

    float ti,tf;
    lista_intervalos tiemposHabla;
    intervalo tiempo;

    //Obtener Valores del Archivo habla_spkX.txt
    if (fl.is_open()){
        while(!fl.eof())
        {

            fl >> ti;
            fl >> tf;
            tiempo = make_tuple(ti,tf);
            tiemposHabla.push_back(tiempo);

        }
        fl.close();
    }
    //Convertir en Mascara de Silencios los intervalos del archivo
    vector<bool> mascara = enmascarar(vec.size()/freq,tiemposHabla);

    negacionLogica(mascara);

    lista_intervalos tiemposSilencios = silencios(vec,prof,freq,umbralSilencio);

    vector<bool> mascaraSilencios = enmascarar(vec.size()/freq,tiemposSilencios);

    int vp = 0;
    int vn = 0;
    int fp = 0;
    int fn = 0;

    //Comparar mascaras y detectar valores
    for(int i=0;i<mascara.size();i++){
        if (mascara[i] && mascaraSilencios[i]){
            vp++;
        }
        if (!mascara[i] && !mascaraSilencios[i]){
            vn++;
        }
        if (!mascara[i] && mascaraSilencios[i]){
            fp++;
        }
        if (mascara[i] && !mascaraSilencios[i]){
            fn++;
        }
    }

    //Calcular F1-Score
    float precision = (float)vp / (float)(vp+fp);
    float recall = (float)vp / (float)(vp+fn);

    float F1 = (precision*recall)/(precision+recall);

    F1=F1*2;

    return F1;
}

vector<bool> enmascarar(int duracion, lista_intervalos tiempos){
    vector<bool> mascara;
    bool eshabla = false;
    for(int i=0;i<duracion*100;i++){
        eshabla=false;
        for (int j=0;j<tiempos.size() && !eshabla;j++) {

             eshabla = eshabla || ( (float)i/100 >= (get<0>(tiempos[j])) && (float)i/100 < (get<1>(tiempos[j])) );
        }
        mascara.push_back(eshabla);

    }
    return mascara;
}
void negacionLogica(vector<bool> &mascara){
    for(int i=0;i<mascara.size();i++){
        mascara[i]=!mascara[i];
    }

}

/************************** EJERCICIO resultadoFinal **************************/

float resultadoFinal(sala m, int freq, int prof, int umbralSilencio){
    float suma=0;
    for(int i=0;i<m.size();i++ ){
        suma += compararSilencios(m[i],freq,prof,i,umbralSilencio);
    }
    suma = suma / (float)m.size();

    return suma;
}

/************************** EJERCICIO sinSilencios **************************/
audio sinSilencios ( audio vec , int freq , int prof , int umbral ){

    audio result;
    lista_intervalos listaSilencios = silencios(vec,prof,freq,umbral);
    bool esSilencio=false;
    for (int i=0;i<vec.size();i++){
        esSilencio=false;
        for(int j=0;j<listaSilencios.size();j++){
            esSilencio = esSilencio || ( i >= (int)redondear(get<0>(listaSilencios[j])*freq) && i < (int)redondear(get<1>(listaSilencios[j])*freq) );
        }
        if(!esSilencio){
        result.push_back(vec[i]);
        }
    }

    return result;

}


/************************** EJERCICIO encontrarAparicion **************************/

int encontrarAparicion(audio x, audio y){
    int r=0;
    float corR = correlacion(subAudio(x,r,y.size()),y);
    for(int i=1; i<x.size()-y.size();i++){

            if(correlacion(subAudio(x,i,y.size()),y) > corR) {
                r = i;
                corR = correlacion(subAudio(x,r,y.size()),y);
            }
    }


    return r;
}

/************************** EJERCICIO medirLaDistancia **************************/
locutor medirLaDistancia(sala m, audio frase, int freq, int prof){
    int r=0;
    vector<int> apariciones;
    apariciones.push_back(encontrarAparicion(m[r],frase));
    int intenR = intensidadMedia(subAudio(m[r],apariciones[r],frase.size()));
    for (int i=1;i<m.size();i++){
        apariciones.push_back(encontrarAparicion(m[i],frase));
       if (intenR < intensidadMedia(subAudio(m[i],apariciones[i],frase.size()))){
           r=i;
           intenR = intensidadMedia(subAudio(m[i],apariciones[i],frase.size()));
       }

    }
    //DistanciasAP
    lista_distancias distancias;
    float res;
    int aparicionP1 = abs(apariciones[r]);
    int aparicionP2;
    for (int i=0;i<m.size();i++){
        aparicionP2 = abs(apariciones[i]);
        res = (aparicionP1-aparicionP2)* ( (float)VELOCIDAD_SONIDO /(float)freq );
        distancias.push_back(res);
    }

    locutor out;

    out = make_tuple(r,distancias);

    return out;
}

