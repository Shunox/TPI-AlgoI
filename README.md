GIT PROJECT

Mi recomendacion es clonarse el repositorio, parate en una carpeta y -->
git clone https://gitlab.com/Shunox/TPI-AlgoI.git

luego crearse una branch para trabajar en una funcion ej "hayQuilombo" -->
git checkout -b hayQuilombo

eso va a pararte en la branch hayQuilombo de paso, 
y ahi trabajas con clion o el IDE que quieras, editas todo lo que queres y guardas.

luego para realizar commits agregas solo los archivos que modificaste, 
van a aparecer otros de cmake pero esos no los agreges porque siempre son modificados durante compilaciones anteriores...
Ej: --> 
git add src/solucion.cpp -->
git add src/salaDeReunion

luego el commit -->
git commit -m "Mensaje descriptivo de lo que hiciste"

y cuando ya no modifiques nada y estes comodo con los commit (capaz es uno solo o muchos depende cuantas veces comitees durante la programacion) o ya hayas terminado la funcion,

mergeas la branch con tu master ej --> git checkout master --> git merge hayQuilombo --> seguramente hace fastFoward (no hay conflictos que requieran merge) hasta este paso...

luego la pusheas --> git push origin master , en caso de que yo justo haya modificado la misma funcion o mismas lineas al mismo tiempo, te va a tirar error y tenes que hacer un pull.

git pull origin master --> te pulea, te va a pedir merge manual, lo haces y luego

---> git push origin master.

y listo esta subido, si queres borrar la branch en tu repo hace un git branch -d hayQuilombo

Es basicamente para que quede ordenado y una historia prolija,
pero sino podes hacerlo como quieras, no pasa nada total siempre se puede volver si cometes un error,
a menos que hagas --force o -f o borres todo ;D.

Saludos a editores de este GIT

Mateo.